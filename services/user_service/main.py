from config.config import Config
from src.service import UserService
from src.http_server import Server


def main():
    user_service = UserService(Config())
    server = Server('user_service', user_service=user_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
