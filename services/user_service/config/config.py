import os

class Config:
    def __init__(self):
        self.data_path = os.environ['USER_DATA_FILE']
