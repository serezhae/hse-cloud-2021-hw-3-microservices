import json

import pandas as pd
from config.config import Config


def read_csv_data(path):
    res = pd.read_csv(path)
    return res.to_dict('records')


class UserService:
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self, config: Config):
        self._data = dict()
        data_path = config.data_path
        data = pd.read_csv(data_path).to_dict('records')
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}

    def get_user_data(self, user_id):
        return json.dumps(self._data.get(user_id))

