from flask import Flask, request

from .service import UserService


class Server(Flask):
    def __init__(self, name: str, user_service: UserService):
        super().__init__(name)
        self._user_service = user_service
        urls = [
            ('/search', self.search, {}),
            ('/ping', self.ping, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        user_id = int(request.args.get('user_id'))
        sr = self._user_service.get_user_data(user_id)
        return sr

    def ping(self):
        return 'pong'

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
