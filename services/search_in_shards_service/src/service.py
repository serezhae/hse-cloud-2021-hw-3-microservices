import pandas as pd
import json

import asyncio
import aiohttp

from config.config import Config


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class SearchInShardsService:
    def __init__(self, config: Config):
        self.DOCS_COLUMNS = ['document', 'key', 'key_md5']
        self._data = None
        self._shards = config.shards

    def _build_tokens_count(self, search_text):
        tokens = search_text.split()
        res = self._data['document'].apply(lambda x: stupid_count_tokens(tokens, x))
        res.name = None
        return res

    def _get_gender_mask(self, user_data=None):
        ud = user_data.get('gender', 'null') if user_data is not None else 'non-existing gender'
        return self._data['gender'].apply(lambda x: stupid_count_tokens([ud], x))

    def _get_age_mask(self, user_data=None):
        user_age = int(user_data['age']) if user_data is not None else -1
        return self._data.apply(lambda x: x['age_from'] <= user_age <= x['age_to'], axis=1)

    def _sort_by_rating_and_tokens(self, rating, tokens_count, key_md5):
        df = pd.concat([tokens_count, rating, key_md5], axis=1)
        return df.sort_values([0, 1, 'key_md5'], ascending=[False, False, False])

    async def _perform_request(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url=url) as response:
                return pd.DataFrame(await response.json(content_type=None))

    async def _get_shards_responses(self, search_text, user_data=None, limit=10):
        query = f'/search?text={search_text}&user_data={json.dumps(user_data)}&limit={limit}'
        urls = [url + query for url in self._shards]
        return await asyncio.gather(*[self._perform_request(url) for url in urls])

    def get_search_data(self, search_text, user_data=None, limit=10):
        user_data = json.loads(user_data) if user_data is not None else None
        shards_responses = asyncio.run(self._get_shards_responses(search_text, user_data, limit))
        self._data = pd.concat(shards_responses)  # possible data race in case of multi thread/async usage
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique
        if search_text is None or search_text == '':
            return json.dumps(pd.DataFrame([], columns=self.DOCS_COLUMNS).to_dict('records'))
        tokens_count = self._build_tokens_count(search_text)
        gender_mask = self._get_gender_mask(user_data)
        age_mask = self._get_age_mask(user_data)
        rating = gender_mask + age_mask
        df = self._sort_by_rating_and_tokens(rating, tokens_count, self._data['key_md5'])
        result = self._data.loc[df.head(limit).index]
        return result[self.DOCS_COLUMNS].to_dict('records')
