from config.config import Config
from src.service import SearchInShardsService
from src.http_server import Server


def main():
    search_in_shards = SearchInShardsService(config=Config())
    server = Server('search_in_shards_service', service=search_in_shards)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
