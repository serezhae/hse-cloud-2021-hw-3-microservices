import os

class Config:
    def __init__(self):
        self.n_shards = int(os.environ['N_SHARDS'])
        self.shards = []
        for i in range(1, self.n_shards + 1):
            self.shards.append(os.environ[f'SHARD_{i}_URI'])
