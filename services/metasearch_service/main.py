from src.http_server import Server
from src.service import MetaSearchService
from config.config import Config


def main():
    metasearch = MetaSearchService(config=Config())
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
