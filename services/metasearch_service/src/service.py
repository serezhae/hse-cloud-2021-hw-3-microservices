import requests
from typing import List, Dict

from config.config import Config


class MetaSearchService:
    def __init__(self, config: Config) -> None:
        self._config = config

    def get_user_data(self, user_id):
        query_url = self._config.user_service_uri + f'/search?user_id={user_id}'
        return requests.get(query_url).text

    def get_search_data(self, search_text, user_data, limit):
        query_url = self._config.shards_search_uri + f'/search?text={search_text}&user_data={user_data}&limit={limit}'
        return requests.get(query_url).json()

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = self.get_user_data(user_id)  # {'gender': ..., 'age': ...}
        search_data = self.get_search_data(search_text, user_data, limit)
        return search_data
