import os

class Config:
    def __init__(self):
        self.shards_search_uri = os.environ['SHARDS_SEARCH_URI']
        self.user_service_uri = os.environ['USER_SERVICE_URI']
