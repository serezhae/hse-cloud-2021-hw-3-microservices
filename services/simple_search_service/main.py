from config.config import Config
from src.service import SimpleSearchService
from src.http_server import Server


def main():
    simple_search = SimpleSearchService(config=Config())
    server = Server('simple_search_service', service=simple_search)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
