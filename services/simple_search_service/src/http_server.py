import json

from flask import Flask, request
from src.service import SimpleSearchService

class Server(Flask):
    def __init__(self, name: str, service: SimpleSearchService):
        super().__init__(name)
        self._service = service
        urls = [
            ('/search', self.search, {}),
            ('/ping', self.ping, {})
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        text = request.args.get('text')
        user_data = json.loads(request.args.get('user_data'))
        limit = int(request.args.get('limit'))
        result = self._service.get_search_data(text, user_data, limit)
        return json.dumps(result)

    def ping(self):
        return 'pong'

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
