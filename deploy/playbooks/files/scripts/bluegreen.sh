#!/bin/bash

if [ "$( sudo docker container inspect -f '{{.State.Running}}' service_blue )" == "true" ]; then
  OLD=service_blue
  NEW=service_green
else
  OLD=service_green
  NEW=service_blue
fi
echo "$OLD $NEW"
IMAGE=$1
ENV=$2
sudo docker run -d $ENV --network web-gateway --name $NEW $IMAGE:latest
for i in {1..5}
do
  sleep 1
  echo $i
  if [ "$( sudo docker container inspect -f '{{.State.Running}}' $NEW )" == "true" ]; then
    echo "starting being cool..."
    sleep 2
    sudo docker exec reverse-proxy sed -i "s/$OLD/$NEW/g" /etc/nginx/conf.d/default.conf
    sudo docker exec reverse-proxy nginx -s reload
    sleep 2
    sudo docker stop $OLD
    sudo docker container prune -f
    echo "SUCCESS!!!"
    exit 0
  fi
done
exit 1
